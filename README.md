# Java REST service that consumes the unofficial Overwatch API and provides the data via it’s own REST API
![Requirements](requirements.png)

# What has been done
All required parts and bonus parts: 

1.  Use Swagger <https://swagger.io> to expose hero and ability end-points.
2.  Unit tests.
3.  Data persistence with H2.


# Prerequisite

1. Installations: Install latest Oracle JDK 8, download at <http://www.oracle.com/technetwork/java/javase/downloads/index.html>
2. Install Gradle 5.4.1 or higher

# Run the application

1.  Use the command `gradle build` in the project folder to build and run all unit tests.
2.  Use the command `gradle bootRun` in the project folder to run the app.
3.  The application runs at the port 8080. Use web browser to make a request to an API: <http://localhost:8080/api/heroes>
4.  Access the Swagger API documentation at <http://localhost:8080/swagger-ui.html>
5.  Access the embedded H2 database console at <http://localhost:8080/h2-console>


# Notes about technologies
*   Spring Boot 2.1.6 for the app with Spring Rest.
*   Embedded database H2, Spring Data JPA
*   Unit test with jmockit for mocking.

# Notes about code architecture
*   All configuration classes are in the package `com.mycompany.restoverwatchapi.config`. All resource end-points are in the classes in the package `com.mycompany.restoverwatchapi.controller`
