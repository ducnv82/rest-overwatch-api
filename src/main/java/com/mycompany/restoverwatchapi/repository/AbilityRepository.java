package com.mycompany.restoverwatchapi.repository;

import com.mycompany.restoverwatchapi.entity.AbilityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbilityRepository extends JpaRepository<AbilityEntity, Long> {
}
