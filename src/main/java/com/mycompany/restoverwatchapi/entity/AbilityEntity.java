package com.mycompany.restoverwatchapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ability")
public class AbilityEntity extends BaseEntity {

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column
    private boolean ultimate;

    @Column(columnDefinition = "TEXT")
    private String url;

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public boolean isUltimate() {
        return ultimate;
    }

    public void setUltimate(final boolean ultimate) {
        this.ultimate = ultimate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
