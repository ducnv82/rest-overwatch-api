package com.mycompany.restoverwatchapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "hero")
public class HeroEntity extends BaseEntity {

    @Column(name = "real_name", columnDefinition = "TEXT")
    private String realName;

    @Column
    private int health;

    @Column
    private int armour;

    @Column
    private int shield;

    public String getRealName() {
        return realName;
    }

    public void setRealName(final String realName) {
        this.realName = realName;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(final int health) {
        this.health = health;
    }

    public int getArmour() {
        return armour;
    }

    public void setArmour(final int armour) {
        this.armour = armour;
    }

    public int getShield() {
        return shield;
    }

    public void setShield(final int shield) {
        this.shield = shield;
    }
}
