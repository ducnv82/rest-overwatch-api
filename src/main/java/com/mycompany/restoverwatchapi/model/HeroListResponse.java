package com.mycompany.restoverwatchapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

public class HeroListResponse {

    private int total;

    @JsonAlias("data")
    private List<Hero> heroes;

    public HeroListResponse() {}

    public HeroListResponse(final int total, final List<Hero> heroes) {
        this.total = total;
        this.heroes = heroes;
    }

    public int getTotal() {
        return total;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }
}
