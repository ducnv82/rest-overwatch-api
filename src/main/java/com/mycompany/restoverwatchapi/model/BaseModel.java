package com.mycompany.restoverwatchapi.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public abstract class BaseModel {

    @ApiModelProperty(example = "1", notes = "id", dataType = "long")
    protected long id;

    @ApiModelProperty(example = "Tom Hanks", notes = "Name")
    protected String name;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
