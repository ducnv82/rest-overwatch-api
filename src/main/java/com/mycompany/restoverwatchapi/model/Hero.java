package com.mycompany.restoverwatchapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.mycompany.restoverwatchapi.entity.HeroEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import static java.util.Collections.emptyList;
import static org.springframework.beans.BeanUtils.copyProperties;

@ApiModel
public class Hero extends BaseModel {

    @ApiModelProperty(example = "Tom Real Cruise", notes = "Real name")
    @JsonAlias("real_name")
    private String realName;

    @ApiModelProperty(example = "200", notes = "Health metric")
    private int health;

    @ApiModelProperty(example = "100", notes = "Armour metric")
    private int armour;

    @ApiModelProperty(example = "300", notes = "Shield metric")
    private int shield;

    @ApiModelProperty(notes = "Abilities list")
    private List<Ability> abilities = emptyList();

    public String getRealName() {
        return realName;
    }

    public int getHealth() {
        return health;
    }

    public int getArmour() {
        return armour;
    }

    public int getShield() {
        return shield;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setRealName(final String realName) {
        this.realName = realName;
    }

    public void setHealth(final int health) {
        this.health = health;
    }

    public void setArmour(final int armour) {
        this.armour = armour;
    }

    public void setShield(final int shield) {
        this.shield = shield;
    }

    public void setAbilities(final List<Ability> abilities) {
        this.abilities = abilities;
    }

    public HeroEntity toHeroEntity() {
        final HeroEntity heroEntity = new HeroEntity();
        copyProperties(this, heroEntity);
        return heroEntity;
    }
}
