package com.mycompany.restoverwatchapi.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.mycompany.restoverwatchapi.entity.AbilityEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import static org.springframework.beans.BeanUtils.copyProperties;

@ApiModel
public class Ability extends BaseModel {

    @ApiModelProperty(example = "Descrition 1", notes = "Description")
    private String description;

    @ApiModelProperty(example = "true", notes = "Is ultimate or not")
    @JsonAlias("is_ultimate")
    private boolean ultimate;

    @ApiModelProperty(example = "https://bitbucket.org/ducnv82/", notes = "URL")
    private String url;

    public String getDescription() {
        return description;
    }

    public boolean isUltimate() {
        return ultimate;
    }

    public String getUrl() {
        return url;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setUltimate(final boolean ultimate) {
        this.ultimate = ultimate;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public AbilityEntity toAbilityEntity() {
        final AbilityEntity abilityEntity = new AbilityEntity();
        copyProperties(this, abilityEntity);
        return abilityEntity;
    }
}
