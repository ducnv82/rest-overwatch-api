package com.mycompany.restoverwatchapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

public class AbilityListResponse {

    private int total;

    @JsonAlias("data")
    private List<Ability> ablities;

    public AbilityListResponse() {}

    public AbilityListResponse(final int total, final List<Ability> ablities) {
        this.total = total;
        this.ablities = ablities;
    }

    public int getTotal() {
        return total;
    }

    public List<Ability> getAblities() {
        return ablities;
    }
}
