package com.mycompany.restoverwatchapi.controller;

import java.util.List;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.model.Hero;
import com.mycompany.restoverwatchapi.service.HeroService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/heroes")
public class HeroController {

    @Autowired
    private HeroService heroService;

    @GetMapping
    @ApiOperation(value = "Find all heroes", responseContainer = "List", response = Hero.class, produces = "application/json")
    public List<Hero> findAllHeroes() {
        return heroService.findAllHeroes();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find a hero by id", response = Hero.class, produces = "application/json")
    public Hero findHeroById(@PathVariable final String id) {
        return heroService.findHeroById(id);
    }

    @GetMapping("/{id}/abilities")
    @ApiOperation(value = "Find all abilities of a hero by hero id", responseContainer = "List", response = Ability.class, produces = "application/json")
    public List<Ability> findHeroAblitiesByHeroId(@PathVariable final String id) {
        return heroService.findHeroAblitiesByHeroId(id);
    }
}
