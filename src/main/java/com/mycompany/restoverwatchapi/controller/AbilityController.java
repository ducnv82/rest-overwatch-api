package com.mycompany.restoverwatchapi.controller;

import java.util.List;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.service.AbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("Ability APIs")
@RestController
@RequestMapping("/api/abilities")
public class AbilityController {

    @Autowired
    private AbilityService abilityService;

    @GetMapping
    @ApiOperation(value = "Find all abilities", responseContainer = "List", response = Ability.class, produces = "application/json")
    public List<Ability> findAllAbilities() {
        return abilityService.findAllAbilities();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find an ability by id", response = Ability.class, produces = "application/json")
    public Ability findAbilityById(@PathVariable final String id) {
        return abilityService.findAbilityById(id);
    }
}
