package com.mycompany.restoverwatchapi.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.model.Hero;

public interface HeroService {

    String HEROES_ENDPOINT = "https://overwatch-api.net/api/v1/hero/";

    List<Hero> findAllHeroes();

    Hero findHeroById(@Valid @NotBlank String id);

    List<Ability> findHeroAblitiesByHeroId(@Valid @NotBlank String id);
}
