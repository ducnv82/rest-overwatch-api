package com.mycompany.restoverwatchapi.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.restoverwatchapi.model.Ability;

public interface AbilityService {

    String ABILITIES_ENDPOINT = "https://overwatch-api.net/api/v1/ability/";

    List<Ability> findAllAbilities();

    Ability findAbilityById(@Valid @NotBlank String id);

}
