package com.mycompany.restoverwatchapi.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.model.AbilityListResponse;
import com.mycompany.restoverwatchapi.repository.AbilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;

@Service
@Validated
public class AbilityServiceImpl implements AbilityService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpEntity<String> httpHeaderEntity;

    @Autowired
    private AbilityRepository abilityRepository;

    @Override
    @Transactional
    public List<Ability> findAllAbilities() {
        final List<Ability> abilities = restTemplate.exchange(ABILITIES_ENDPOINT, GET, httpHeaderEntity, AbilityListResponse.class).getBody().getAblities();

        saveToDatabase(abilities);

        return abilities;
    }


    @Override
    public Ability findAbilityById(@Valid @NotBlank final String id) {
        return restTemplate.exchange(ABILITIES_ENDPOINT + id, GET, httpHeaderEntity, Ability.class).getBody();
    }

    private void saveToDatabase(final List<Ability> abilities) {
        abilityRepository.deleteAllInBatch();
        for (Ability ability : abilities) {
            abilityRepository.save(ability.toAbilityEntity());
        }
    }
}
