package com.mycompany.restoverwatchapi.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.model.Hero;
import com.mycompany.restoverwatchapi.model.HeroListResponse;
import com.mycompany.restoverwatchapi.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;

import static java.util.Collections.emptyList;
import static org.springframework.http.HttpMethod.GET;

@Service
@Validated
public class HeroServiceImpl implements HeroService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpEntity<String> httpHeaderEntity;

    @Autowired
    private HeroRepository heroRepository;

    @Override
    @Transactional
    public List<Hero> findAllHeroes() {
        final List<Hero> heroes = restTemplate.exchange(HEROES_ENDPOINT, GET, httpHeaderEntity, HeroListResponse.class).getBody().getHeroes();

        saveToDatabase(heroes);

        return heroes;
    }

    @Override
    public Hero findHeroById(@Valid @NotBlank final String id) {
        return restTemplate.exchange(HEROES_ENDPOINT + id, GET, httpHeaderEntity, Hero.class).getBody();
    }

    @Override
    public List<Ability> findHeroAblitiesByHeroId(@Valid @NotBlank final String id) {
        final Hero hero = findHeroById(id);
        return hero == null ? emptyList() : hero.getAbilities();
    }

    private void saveToDatabase(final List<Hero> heroes) {
        heroRepository.deleteAllInBatch();
        for (Hero hero : heroes) {
            heroRepository.save(hero.toHeroEntity());
        }
    }
}
