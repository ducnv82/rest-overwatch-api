package com.mycompany.restoverwatchapi.service;

import java.util.Arrays;
import java.util.List;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.model.Hero;
import com.mycompany.restoverwatchapi.model.HeroListResponse;
import com.mycompany.restoverwatchapi.repository.HeroRepository;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static com.mycompany.restoverwatchapi.service.HeroService.HEROES_ENDPOINT;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpMethod.GET;

public class HeroServiceTest extends BaseTest {

    @Tested
    private HeroServiceImpl heroService;

    @Injectable
    private RestTemplate restTemplate;

    @Injectable
    private HttpEntity<String> httpHeaderEntity;

    @Injectable
    private HeroRepository heroRepository;

    private Hero mockedHero;

    @Override
    @Before
    public void setUp() {
        super.setUp();

        mockedHero = new Hero();
        mockedHero.setId(100);
        mockedHero.setArmour(100);
        mockedHero.setHealth(200);
        mockedHero.setName("hero 1");
        mockedHero.setRealName("Hero 1 real name");
        mockedHero.setShield(300);
        mockedHero.setAbilities(singletonList(mockedAbility));
    }

    @Test
    public void testFindAllHeroes() {
        final List<Hero> mockedHeroes = Arrays.asList(mockedHero);

        new Expectations() {
            {
                restTemplate.exchange(HEROES_ENDPOINT, GET, httpHeaderEntity, HeroListResponse.class);
                result = ResponseEntity.ok(new HeroListResponse(1, mockedHeroes));
            }
        };

        final List<Hero> heroes = heroService.findAllHeroes();
        assertEquals(heroes.size(), mockedHeroes.size());
        assertEquals(mockedHeroes.get(0).getArmour(), heroes.get(0).getArmour());
        assertEquals(mockedHeroes.get(0).getHealth(), heroes.get(0).getHealth());
        assertEquals(mockedHeroes.get(0).getId(), heroes.get(0).getId());
        assertEquals(mockedHeroes.get(0).getName(), heroes.get(0).getName());
        assertEquals(mockedHeroes.get(0).getRealName(), heroes.get(0).getRealName());
        assertEquals(mockedHeroes.get(0).getShield(), heroes.get(0).getShield());
    }

    @Test
    public void testFindHeroById() {
        new Expectations() {
            {
                restTemplate.exchange(HEROES_ENDPOINT + mockedHero.getId(), GET, httpHeaderEntity, Hero.class);
                result = ResponseEntity.ok(mockedHero);
            }
        };

        final Hero hero = heroService.findHeroById(String.valueOf(mockedHero.getId()));
        assertEquals(mockedHero.getArmour(), hero.getArmour());
        assertEquals(mockedHero.getHealth(), hero.getHealth());
        assertEquals(mockedHero.getId(), hero.getId());
        assertEquals(mockedHero.getName(), hero.getName());
        assertEquals(mockedHero.getRealName(), hero.getRealName());
        assertEquals(mockedHero.getShield(), hero.getShield());
    }

    @Test
    public void testFindHeroAblitiesByHeroId() {
        new Expectations() {
            {
                restTemplate.exchange(HEROES_ENDPOINT + mockedHero.getId(), GET, httpHeaderEntity, Hero.class);
                result = ResponseEntity.ok(mockedHero);
            }
        };

        final Ability ability = heroService.findHeroAblitiesByHeroId(String.valueOf(mockedHero.getId())).get(0);
        assertEquals(mockedAbility.getDescription(), ability.getDescription());
        assertEquals(mockedAbility.getName(), ability.getName());
        assertEquals(mockedAbility.getId(), ability.getId());
        assertEquals(mockedAbility.getUrl(), ability.getUrl());
    }

    @Test
    public void testFindHeroAblitiesByHeroId_empty() {
        final String mockerHeroId = "-100";

        new Expectations() {
            {
                restTemplate.exchange(HEROES_ENDPOINT + mockerHeroId, GET, httpHeaderEntity, Hero.class);
                result = ResponseEntity.ok(null);
            }
        };

        final List<Ability> abilities = heroService.findHeroAblitiesByHeroId(mockerHeroId);
        assertEquals(0, abilities.size());
    }
}
