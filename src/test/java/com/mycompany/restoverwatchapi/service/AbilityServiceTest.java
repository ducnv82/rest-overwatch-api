package com.mycompany.restoverwatchapi.service;

import java.util.Arrays;
import java.util.List;

import com.mycompany.restoverwatchapi.model.Ability;
import com.mycompany.restoverwatchapi.model.AbilityListResponse;
import com.mycompany.restoverwatchapi.repository.AbilityRepository;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static com.mycompany.restoverwatchapi.service.AbilityService.ABILITIES_ENDPOINT;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpMethod.GET;

public class AbilityServiceTest extends BaseTest {

    @Tested
    private AbilityServiceImpl abilityService;

    @Injectable
    private RestTemplate restTemplate;

    @Injectable
    private HttpEntity<String> httpHeaderEntity;

    @Injectable
    private AbilityRepository abilityRepository;

    @Test
    public void testFindAllAbilities() {
        final List<Ability> mockedAbilities = Arrays.asList(mockedAbility);

        new Expectations() {
            {
                restTemplate.exchange(ABILITIES_ENDPOINT, GET, httpHeaderEntity, AbilityListResponse.class);
                result = ResponseEntity.ok(new AbilityListResponse(1, mockedAbilities));
            }
        };

        final List<Ability> abilities = abilityService.findAllAbilities();
        assertEquals(mockedAbilities.size(), abilities.size());
        assertEquals(mockedAbilities.get(0).getDescription(), abilities.get(0).getDescription());
        assertEquals(mockedAbilities.get(0).getId(), abilities.get(0).getId());
        assertEquals(mockedAbilities.get(0).getName(), abilities.get(0).getName());
        assertEquals(mockedAbilities.get(0).getUrl(), abilities.get(0).getUrl());
    }

    @Test
    public void testFindAbilityById() {
        new Expectations() {
            {
                restTemplate.exchange(ABILITIES_ENDPOINT + mockedAbility.getId(), GET, httpHeaderEntity, Ability.class);
                result = ResponseEntity.ok(mockedAbility);
            }
        };

        final Ability ability = abilityService.findAbilityById(String.valueOf(mockedAbility.getId()));
        assertEquals(mockedAbility.getDescription(), ability.getDescription());
        assertEquals(mockedAbility.getId(), ability.getId());
        assertEquals(mockedAbility.getName(), ability.getName());
        assertEquals(mockedAbility.getUrl(), ability.getUrl());
    }
}
