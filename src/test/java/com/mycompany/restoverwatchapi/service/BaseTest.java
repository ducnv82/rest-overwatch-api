package com.mycompany.restoverwatchapi.service;

import com.mycompany.restoverwatchapi.model.Ability;
import org.junit.Before;

public abstract class BaseTest {

    protected Ability mockedAbility;

    @Before
    public void setUp() {
        mockedAbility = new Ability();
        mockedAbility.setDescription("Ability description");
        mockedAbility.setId(200);
        mockedAbility.setName("Ablility name 1");
        mockedAbility.setUltimate(true);
        mockedAbility.setUrl("https://bitbucket.org/ducnv82");
    }
}
